import { h, Component } from 'preact';
import { Link } from 'preact-router/match';

import Button from './../Button';
import style from './style';
import { APPLY_FILTER } from '../../actions';

export default class ListView extends Component {
	constructor(props) {
    super(props);
    this.state = {
      selectedFilter: null,
      selectedFilterIds: null,
    };
  }
  
  componentDidMount() {
    var filterSet = {};
    
    Object.keys(this.props.filters).forEach(item => {
      filterSet[item] = [];
    });

    this.setState({
      selectedFilter: this.props.filterKeys.filters[0].key,
      selectedFilterIds: filterSet,
    });
  }

	render(props, state) {
		return (
			<div class={style.filtersPanelWrapper}>
				<header>
          <Button clickHandler={props.exitFiltersPanel} 
                  style={{background: 'transparent', color: '#fff', width: 100, height: 56, lineHeight: '56px', fontSize: 14,}} 
                  name={'Filters'} 
                  icon={'zmdi zmdi-arrow-left'} />
        </header>
        <div class={style.filtersPanelContent}>
          <div class={'filtersPanelCat'}>
            <ul>
              { props.filterKeys.filters.map((item, index) => {
                return <li key={index} 
                           class={state.selectedFilter === item.key ? 'active' : ''} 
                           onClick={this.handleFilterCat.bind(this, item)}>{item.title}</li>
              }) }
            </ul>
          </div>
          <div class={style.filtersPanelFilters}>
            <ul>
              { state.selectedFilter ? 
                props.filters[state.selectedFilter].map((item, index) => {
                  return (
                    <li key={index}>
                      <label for={item.id}>
                        <input type={'checkbox'} 
                               autocomplete={'off'}
                              //  checked={state.selectedFilterIds[state.selectedFilter].indexOf(item)}
                               onChange={this.handleFilterSelect.bind(this, state.selectedFilter, item)} 
                               name={item.id} 
                               value={item.id} />
                        {item.name}
                      </label>
                    </li>
                  )
                }) : null
              }
            </ul>
          </div>
        </div>
        <footer>
          <Button clickHandler={props.exitFiltersPanel} 
                  style={{
                    background: 'rgb(54, 207, 209)', color: '#fff', width: 160, height: 44, lineHeight: '44px', 
                    fontSize: 14, padding: '0 40px', borderRadius: 4, float: 'right', fontWeight: '600',
                  }} 
                  name={'Apply'} />
        </footer>
      </div>
		);
  }

  handleFilterCat = (filter) => {
    this.setState({
      selectedFilter: filter.key
    });
  }

  handleFilterSelect = (cat, filter, event) => {
    this.props.dispatcher(APPLY_FILTER, {
      cat, 
      filter
    });
  }
}