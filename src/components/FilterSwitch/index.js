import { h, Component } from 'preact';
import { Link } from 'preact-router/match';

import Button from './../Button';
import style from './style';

export default class FilterSwitches extends Component {
	constructor(props) {
		super(props);
	}

	render(props, state) {
    let buttonStyle = {
      width: '50%',
      background: '#fff',
      color: '#000',
      border: '1px solid #eaeaea',
    };

		return (
			<div class={style.filterSwitchesWrapper}>
				<Button clickHandler={props.showFiltersPanel} style={buttonStyle} name={'Filter'} icon={'zmdi zmdi-filter-list'} />
        <Button clickHandler={props.showSortPanel} style={buttonStyle} name={'Sort'} icon={'zmdi zmdi-sort-amount-asc'} />
      </div>
		);
	}
}