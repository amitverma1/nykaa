import { h, Component } from 'preact';
import { Link } from 'preact-router/match';

import ListView from './../ListView';
import CartItem from './../CartItem';
import Button from './../Button';
import style from './style';

export default class Cart extends Component {
	constructor(props) {
    super(props);
    this.state = {
      total: 0,
      subTotal: 0,
      discount: 0,
      shipping: 0
    }
  }

  componentDidMount() {
    this.getCartDetails();
  }

  componentWillReceiveProps(nextProps, nextState) {
    this.getCartDetails();
  }

	render(props, state) {
		return (
			<div class={style.filtersPanelWrapper}>
				<header>
          <Button clickHandler={props.exitCart} 
                  style={{background: 'transparent', color: '#fff', width: 140, height: 56, lineHeight: '56px', fontSize: 14,}} 
                  name={'Shopping Bag'} 
                  icon={'zmdi zmdi-arrow-left'} />
        </header>
        <div class={style.filtersPanelContent}>
          <div class={style.cartHeader}>
            <span class={style.loginText}>Login to redeem your reward points.</span>
            <Button clickHandler={null} 
                  style={{background: 'rgb(54, 207, 209)', color: '#fff', width: 110, height: 44, lineHeight: '44px', borderRadius: 4, fontSize: 14,}} 
                  name={'Login'} />
          </div>
          <div class={style.productList}>
            { props.list && props.list.length > 0 ?
              <ListView list={props.list} 
                listItem={CartItem}
                dispatcher={props.dispatcher}
                showCart={this.showCart} /> :
              <div class={style.emptyCartWrapper}>
                <span class={style.emptyCartMessage}>{'You currently have no items in your cart. Get shopping. '}</span>
              </div>
            }
            
          </div>
          
          <div class={style.cartHeader}>
            <input type={'text'} placeholder={'Promocode'} />
            <Button clickHandler={null} 
                  style={{background: 'rgb(54, 207, 209)', color: '#fff', width: 110, height: 44, lineHeight: '44px', borderRadius: 4, fontSize: 14,}} 
                  name={'Apply'} />
          </div>

          <div class={style.cartDetails}>
            <div class={style.cartDetailsRow}>
              <span class={style.cartDetailsRowLeft}>{'Bag Total:'}</span>
              <span class={style.cartDetailsRowRight}>{`₹${this.state.total}`}</span>
            </div>

            <div class={style.cartDetailsRow}>
              <span class={style.cartDetailsRowLeft}>{'Bag Discount'}</span>
              <span class={style.cartDetailsRowRight}>{`₹${this.state.discount}`}</span>
            </div>

            <div class={style.cartDetailsRow}>
              <span class={style.cartDetailsRowLeft}>{'SubTotal:'}</span>
              <span class={style.cartDetailsRowRight}>{`₹${this.state.subTotal}`}</span>
            </div>

            <div class={style.cartDetailsRow}>
              <span class={style.cartDetailsRowLeft}>{'Shipping'}</span>
              <span class={style.cartDetailsRowRight}>{`₹${this.state.shipping === 0 ? 'Free' : this.state.shipping}`}</span>
            </div>

            <div class={`${style.cartDetailsRow} ${style.cartDetailsRowTotal}`}>
              <span class={style.cartDetailsRowLeft}>{'Grand Total'}</span>
              <span class={style.cartDetailsRowRight}>{`₹${this.state.shipping + this.state.subTotal}`}</span>
            </div>

          </div>

          <div class={style.checkoutWrapper}>
            <Button clickHandler={null} 
                    style={{background: 'rgb(54, 207, 209)', color: '#fff', width: '100%', height: 48, lineHeight: '48px', borderRadius: 4, fontSize: 14, fontWeight: '700'}} 
                    name={'Checkout'} />
          </div>
        </div>
      </div>
		);
  }

  getCartDetails() {
    var total = this.props.list.reduce((sum, item) => sum + item.quantity * item.mrp, 0);
    var subTotal = this.props.list.reduce((sum, item) => sum + item.quantity * item.price, 0);
    var discount = total - subTotal;

    this.setState({
      total,
      subTotal,
      discount,
      shipping: (subTotal < 500 && subTotal > 0) ? 150 : 0
    })
  }
}