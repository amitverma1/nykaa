export const ENV = process.env.NODE_ENV;
export const constants = {
  ENV_DEV: 'development',
  ENV_PROD: 'production',
  ORIENTATION_PORTRAIT: 'portrait',
  ORIENTATION_LANDSCAPE: 'landscape',
};

export const config = {
  ver: '1.0.0',
  env: ENV,
};

export const links = {
  plp: `http://www.nykaa.com/gludo/products/list?category_id=`,
  
};

export const filtersMap = {
  id: 'id',
  category: 'category_filter',
  brand: 'old_brand_filter',
  price: 'price_filter',
  discount: 'discount_filter',
  gender: 'gender_filter',
  concern: 'concern_filter',
  finish: 'finish_filter',
};

export const resolveFilters = {
  category_filter: 'category_id',
  discount_filter: 'value',
}