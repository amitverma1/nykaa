import { h, Component } from 'preact';
import { Link } from 'preact-router/match';

import style from './style';

export default class Button extends Component {
	constructor(props) {
		super(props);
	}

	render(props, state) {
		return (
			<div class={style.buttonWrapper} style={props.style}>
				<button class={style.genericButton + ' noselect'} onClick={props.clickHandler}>
          { props.icon ? <i class={props.icon} /> : null }
          { props.name }
        </button>
      </div>
		);
	}
}