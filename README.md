# README #

Please go through this README to help setup the app.

### Installation ###

* Git clone to your machine.
* Browse to the folder in CLI and do ```npm install```
* Once it's finished, run ```npm start```
* It should open up in http://localhost:8080?id=18
* If port 8080 is busy, please check CLI, it should mention the port it launched in.

### What has been implemented? ###

* Product Listings Page
* Filters, although not completely as I couldn't get the api response because of CORS
* Shopping Cart
* Add to Bag -> Cart flow.

### What is missing? ###

* Although I got the API from the mobile site, I couldn't get the response because of CORS
* I still make opaque request to the api for initial launch and/or filters

