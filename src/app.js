import { h, Component } from 'preact';
import { Router } from 'preact-router';

import Header from './components/Header';
import ProductList from './containers/ProductList';
import Loader from './components/Loader';
import Store from './Store';

import { APPLY_FILTER, ADD_TO_BAG, CHANGE_QUANTITY, DELETE_ITEM, OPEN_CART } from './actions';

export default class App extends Component {
	constructor() {
		super();
		this.state = {
			cart: null,
			plp: null,
			ready: false,
		};

		this.dispatcher = this.dispatcher.bind(this);
	}

	componentWillMount() {
		this.store = new Store();
		this.start();
	}

	render() {
		return (
			<div id="app">
				<Header cart={this.state.cart} dispatcher={this.dispatcher} />
				{ this.state.ready ? 
						<ProductList ref={p => this.productList = p}
												 products={this.state.plp} 
												 cart={this.state.cart}
												 dispatcher={this.dispatcher} /> : <Loader /> }
			</div>
		);
	}

	start() {
		let init = this.store.initialize();
		init.then(response => {
			this.setState({
				isFacebookApp: response.isFacebookApp,
				isMobileDevice: response.isMobileDevice,
				isTabletDevice: response.isTabletDevice,
				plp: response.plp,
				cart: response.cart,
				ready: true,
			});
		});
	}

	dispatcher(action, data) {
		switch(action) {
			case APPLY_FILTER:
			this.store.applyFilter(data.cat, data.filter).then(plp => {
				this.setState({
					plp
				});
			});
			return;

			case ADD_TO_BAG:
			this.store.addToBag(data.product).then(cart => {
				this.setState({
					cart,
				});
			});
			return;

			case DELETE_ITEM: 
			this.store.deleteItem(data.sku).then(cart => {
				this.setState({
					cart,
				});
			});
			return;

			case CHANGE_QUANTITY:
			this.store.changeQuantity(data.sku, data.quantity).then(cart => {
				this.setState({
					cart,
				});
			});
			return;

			case OPEN_CART:
			this.productList.showCart();
			return;
		}
	}
}
