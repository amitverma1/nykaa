import { h, Component } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';
import { OPEN_CART } from '../../actions';

export default class Header extends Component {
	render(props, state) {
		return (
			<header class={style.header}>
				<div class={style.hamburger}><i class="zmdi zmdi-menu"></i></div>
				<div class={style.headerText}>nykaa</div>
				<div class={style.bag} onClick={this.openCart}>
					{props.cart && props.cart.length ? <div class={style.productsIndicator}>{props.cart.length}</div> : null }
					<i class="zmdi zmdi-shopping-cart"></i>
				</div>
			</header>
		);
	}

	openCart = () => {
		this.props.dispatcher(OPEN_CART);
	}
}
