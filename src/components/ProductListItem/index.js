import { h, Component } from 'preact';
import { Link } from 'preact-router/match';

import * as Helper from '../../helper.js';
import { ADD_TO_BAG } from '../../actions';
import style from './style';

export default class ProductList extends Component {
	constructor(props) {
		super(props);
	}

	render(props, state) {
		return (
			<div class={style.productListItemWrapper}>
				<div class={style.productDetails}>
					<div class={style.imageWrapper}>
						<img src={props.json.media[0].url} />
					</div>
					<div class={style.detailsWrapper}>
						<div class={style.productTitle}>
							<h4>{props.json.title}</h4>
							{props.json.pack_size ? <h5>{props.json.pack_size}</h5> : null }
						</div>
						<div class={style.price + ' noselect'}>
							<div class={style.actualPrice}>{`₹${props.json.price}`}</div>
							{props.json.discount ? <div class={style.mrp}>{`₹${props.json.mrp}`}</div> : null }
						</div>
						<div class={style.stars}>{this.renderStars()}</div>
					</div>
				</div>
				<div class={style.productActions}>
					<div class={style.ctaBuy} onClick={this.handleBuy}>{'Add to Bag'}</div>
				</div>
      </div>
		);
	}

	renderStars() {
		const json = this.props.json;
		const stars = Helper.round(json.star_rating, 0.5);
		const whole = Math.floor(stars);
		const remainder = stars % 1;

		return (
			<div class={style.starsGroup}>
				{Array.apply(0, Array(whole)).map(star => <i class="zmdi zmdi-star"></i>)}
				{remainder > 0 ? <i class="zmdi zmdi-star-half" /> : null}
			</div>
		)
	}

	handleBuy = () => {
		this.props.dispatcher(ADD_TO_BAG, {
			product: this.props.json
		});	
	}
}