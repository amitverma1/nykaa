'use strict';

import { config, links, regex, filtersMap } from './config';
import getProductList from './mock/productList';
import * as Helper from './helper';

export default class Store {
  constructor() {
    this.data = {};
  }

  initialize() {
    this.data = {};
    this.data.cart = [];
    this.data.isMobileDevice = Helper.mobileCheck();
    this.data.isTabletDevice = Helper.tabletCheck();
    this.data.isFacebookApp = Helper.isFacebookApp();

    return new Promise((resolve, reject) => {
      Promise.all([this.getCartItemsFromStorage(), this.getProducts()]).then(json => {
        this.data.plp = getProductList();
        this.data.cart = json[0];
        this.resolveInitPromise(resolve);
      });
    });
  }

  getProducts(updateFilters) {
    let url = updateFilters ? Helper.getFilterListingsLink(this.data.filters) : Helper.getListingsLink();
    return Helper.request(url, {
      mode: 'no-cors',
    });
  }

  getCartItemsFromStorage() {
    return new Promise((resolve, reject) => {
      var cartItems = localStorage.getItem('nykaaCartItems');
      
      try {
        cartItems = JSON.parse(cartItems);
      } catch(e) {
        cartItems = [];
      }

      resolve(cartItems || []);
    });
  }

  setCartItemsToStorage() {
    localStorage.setItem('nykaaCartItems', JSON.stringify(this.data.cart));
  }

  resolveInitPromise(resolve) {
    this.data.filters = Helper.mapFilters(this.data.plp.result.filters);
    resolve({
      plp: this.data.plp,
      cart: this.data.cart,
      isMobileDevice: this.data.isMobileDevice && !this.data.isTabletDevice,
      isTabletDevice: this.data.isTabletDevice,
      isFacebookApp: this.data.isFacebookApp,
    });
  }

  applyFilter(cat, filter) {
    var currentFilter = this.data.filters[filtersMap[cat]];
    var indexOfFilter;

    switch(cat) {
      case 'category': 
        indexOfFilter = Helper.getIndexOfFilter(currentFilter, 'category_id', filter);
        break;
      case 'discount': 
        indexOfFilter = Helper.getIndexOfFilter(currentFilter, 'value', filter);
        break;
      default: 
        indexOfFilter = Helper.getIndexOfFilter(currentFilter, 'id', filter);
        break;
    };

    if (indexOfFilter === -1) currentFilter.push(filter);
    else currentFilter.splice(indexOfFilter, 1);

    return new Promise((resolve, reject) => {
      this.getProducts(true).then(res => {
        this.data.plp = getProductList();
        resolve(this.data.plp);
      });
    });
  }

  addToBag(product) {
    var index = this.data.cart.findIndex(item => item.sku === product.sku);

    if (index !== -1) {
      this.data.cart[index].quantity += 1;
    } else {
      product.quantity = 1;
      this.data.cart.push(product);
    }

    return new Promise((resolve, reject) => {
      this.setCartItemsToStorage();
      resolve(this.data.cart);
    })
  }

  deleteItem(product) {
    var index = this.data.cart.findIndex(item => item.sku === product);

    if (index !== -1) {
      this.data.cart.splice(index, 1);
    }

    return new Promise((resolve, reject) => {
      this.setCartItemsToStorage();
      resolve(this.data.cart);
    });
  }

  changeQuantity(product, quantity) {
    return new Promise((resolve, reject) => {
      var index = this.data.cart.findIndex(item => item.sku === product);
      
      if (index !== -1) {
        this.data.cart[index].quantity = quantity <= 5 ? quantity : 5;
      }
      
      this.setCartItemsToStorage();
      resolve(this.data.cart);
    });
  }
}