import { h, Component } from 'preact';
import { Link } from 'preact-router/match';

import style from './style';

import ListView from './../../components/ListView';
import FiltersPanel from './../../components/FiltersPanel';
import Cart from './../../components/Cart';
import FilterSwitch from './../../components/FilterSwitch';
import ProductListItem from './../../components/ProductListItem';

export default class ProductList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			showFiltersPanel: false,
			showCart: false,
		}
	}

	render(props, state) {
		return (
			<div class={style.productListWrapper} style={{ padding: '60px 0' }}>
        <div class={style.productListContent}>
					<ListView list={props.products.result.products}
										listItem={ProductListItem}
										dispatcher={props.dispatcher}
										showCart={this.showCart} />
					
					{ this.state.showCart ? 
						<Cart list={props.cart}
									exitCart={this.exitCart}
									dispatcher={props.dispatcher} /> : null }					
					
					{ this.state.showFiltersPanel ? 
						<FiltersPanel filters={props.products.result.filters} 
													filterKeys={props.products.result.filter_keys}
													exitFiltersPanel={this.exitFiltersPanel}
													dispatcher={props.dispatcher} /> : null }

					<FilterSwitch showFiltersPanel={this.showFiltersPanel}
												showSortPanel={this.showCart} />
        </div>
      </div>
		);
	}

	exitFiltersPanel = () => {
		this.setState({
			showFiltersPanel: false,
		})
	}

	showFiltersPanel = () => {
		this.setState({
			showFiltersPanel: true,
		})
	}

	exitCart = () => {
		this.setState({
			showCart: false,
		})
	}

	showCart = () => {
		this.setState({
			showCart: true,
		})
	}
}