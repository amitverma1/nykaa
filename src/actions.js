export const APPLY_FILTER = 'app/productList/APPLY_FILTER';
export const ADD_TO_BAG = 'app/productList/ADD_TO_BAG'; 
export const CHANGE_QUANTITY = 'app/cart/CHANGE_QUANTITY';
export const DELETE_ITEM = 'app/cart/DELETE_ITEM';
export const OPEN_CART = 'app/cart/OPEN_CART';