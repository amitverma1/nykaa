import { h, Component } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

import ProductListItem from '../ProductListItem';

export default class ListView extends Component {
	constructor(props) {
		super(props);
	}

	render(props, state) {
		const ListItem = props.listItem;
		return (
			<div class={style.listViewWrapper}>
				{ props.list && props.list.map((item, index) => {
					return this.renderListItem(ListItem, item)
				}) }
      </div>
		);
	}

	renderListItem = (ListItem, item) => {
		return (
			<ListItem {...this.props} json={item} />
		)
	}
}